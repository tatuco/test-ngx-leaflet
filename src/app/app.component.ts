import { Component } from '@angular/core';
import { tileLayer, latLng } from 'leaflet';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  options = {
    layers: [
      tileLayer(
        `http://172.16.40.22:7768/{z}/{x}/{y}.png`,
        {
          maxZoom: 18,
          attribution: '<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }
      )
    ],
    zoom: 5,
    center: latLng(46.879966, -121.726909)
  };
}
